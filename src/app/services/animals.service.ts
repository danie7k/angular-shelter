import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AnimalsList} from '../models/animals-list';
import {Animal} from '../models/animal';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  apiURL = 'http://192.168.88.20:9000/api/animals/';

  constructor(private http: HttpClient) { }

  getAnimalsList() {
    return this.http
      .get<AnimalsList>(this.apiURL);
  }

  getAnimalDetails(id: number) {
    return this.http
      .get<Animal>(this.apiURL + id);
  }
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiUrl = 'http://locahost:8080/users/list';

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http
      .get(this.apiUrl,
        {
          headers: { 'Authorization': 'Basic Q3Jhd2ZvcmQ6bUhMWWUyUmtBYg==' }
        });
  }

  getuserDetails() {

    // TODO

  }

  addUser() {

    // TODO

  }
}

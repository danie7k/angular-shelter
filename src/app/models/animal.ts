export class Animal {
  Id: number;
  Name: string;
  Age: number;
  ShortDescription: string;
  IsAdopted: boolean;
  PhotoUrl: string;
}

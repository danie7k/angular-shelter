import { Component, OnInit } from '@angular/core';
import {Animal} from '../../models/animal';
import {AnimalsService} from '../../services/animals.service';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  animals: Animal[] = [];

  constructor(private animalsService: AnimalsService,
              private usersService: UsersService) {}

  ngOnInit(): void {
    this.usersService
      .getUsers()
      .subscribe(users => {
        console.log(users);
      });

    this.animalsService
      .getAnimalsList()
      .subscribe(animalsList => {
          this.animals = animalsList.Results;
        }
      );
  }

}

import { Component, OnInit } from '@angular/core';
import {Animal} from '../../models/animal';
import {AnimalsService} from '../../services/animals.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  animal: Animal = new Animal();

  constructor(private animalService: AnimalsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.animalService
      .getAnimalDetails(id)
      .subscribe(result => {
        this.animal = result;
      });
  }
}

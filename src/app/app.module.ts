import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { DetailsComponent } from './components/details/details.component';
import {RouterModule, Routes} from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { UsersListComponent } from './components/users-list/users-list.component';

const appRoutes: Routes = [
  { path: 'details/:id', component: DetailsComponent, pathMatch: 'full' },
  { path: '', component: ListComponent, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    ListComponent,
    UsersListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
